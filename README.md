Lusis TranslationBundle is a Symfony vendor bundle that gives you the possibility to translate easily from the profiler, without coding.

Easy for your clients to modify or add translations!

//
//screenshot
//

[Démo screenshot](https://bitbucket.org/lusislu/translationbundle/raw/master/Resources/public/img/screenshot.png)

//
//installation
//

1) //-- add in the composer.json of your project --//

    "require": {
        // ...
        "lusis/translationbundle": "dev-master"
    },
    "repositories": [{
        "type": "vcs",
        "url": "https://bitbucket.org/lusislu/translationbundle.git"
    }]


2) //-- activate the bundle in your app/AppKernel.php --//

    new Lusis\TranslationBundle\LUSISTransBundle(),


3) //-- enter this command in your terminal --//

    php composer update lusis/translationbundle
    
    
4) //-- insert our routes in your app/config/routing_dev.yml file --//

    LUSISTranslation:
        resource: "@LUSISTransBundle/Controller/"
        type:     annotation
    
    
5) //-- insert the service into your app/config/services.yml file --//

    lusis_transbundle.data_collector:
        class: Lusis\TranslationBundle\DataCollector\TranslationsDataCollector
        arguments: ["@translator"]
        tags:
            - { name: data_collector, template: "LUSISTransBundle:profiler:profiler.html.twig", id: "programlusis" }


//
//how to use
//

1) visit a page in app_dev.php and if you've got translated messages missing, defined or fallback, click on the universal access icon in the profiler


2) give the translation for one or more message(s) and submit the form


//
//error codes
//

    200     Translations successfully managed!
    600     File writing with error!
    601     No data sent!
    602     Error managing the xml dom!
    603     Error deleting the cache!
    604     File %filename% not found! You need to create a %filename% file 
            in the app/Resources/translations folder of your app.
    605     Error processing the ajax call!
    606     Invalid csrf token!




