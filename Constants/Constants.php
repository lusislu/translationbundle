<?php

/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 29-11-16
 * Time: 10:41
 */

namespace Lusis\TranslationBundle\Constants;
class Constants
{
    const statusEverythingIsOkCode = 200;
    const statusErrorWritingFileCode = 600;
    const statusEmptyParamsCode = 601;
    const statusErrorManagingTheDomCode = 602;
    const statusErrorDeletingCacheCode = 603;
    const statusErrorFileNotFoundCode = 604;
    const statusErrorAjaxCallCode = 605;
    const statusInvalidCsrfTokenCode = 606;
}