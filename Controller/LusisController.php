<?php

/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 24-11-16
 * Time: 11:51
 */

namespace Lusis\TranslationBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Lusis\TranslationBundle\Constants\Constants;

/**
 * @Route("/lusis/translations")
 * Class LusisController
 * @package Lusis\TranslationBundle\Controller
 */
class LusisController extends Controller
{
    private $catalogue;
    private $locale;
    private $finder;
    private $path;
    private $filename;

    /**
     * @Route("/submit/{type}", name="lusis_translation_validation", requirements={
     *     "type": "defined|missing|fallback"
     * })
     * @Method({"POST", "GET"})
     */
    public function indexAction(Request $request, $type){
        //set basis status
        $this->locale = $request->getLocale();
        $response = [
            'httpcode' => Constants::statusEverythingIsOkCode,
            'message' => $this->getErrorMessage(Constants::statusEverythingIsOkCode, array(), $this->locale)
        ];

        //ERROR 606 /invalid csrf token
        if (!$this->isCsrfTokenValid('lusistranslationbundle', $request->request->get('_csrf_token'))){
            $response['httpcode'] = Constants::statusInvalidCsrfTokenCode;
            $response['message'] = $this->getErrorMessage($response['httpcode'], array(), $this->locale);
        }

        if($response['httpcode'] == Constants::statusEverythingIsOkCode){
            //set necessary elements
            $this->path = $this->get('kernel')->getRootDir().'/Resources/translations/';
            $this->filename = 'messages.'.$this->locale.'.xliff';
            /** @var  $constants \Lusis\TranslationBundle\Constants\Constants */
            $constants = new Constants();
            /** @var  $finder \Symfony\Component\Finder\Finder */
            $this->finder = new Finder();

            //manage POST datas
            $datas = $this->managePostDatas($request->request->get('data'));
            $count = count($datas);

            if($count == 0){
                //ERROR 601 /error empty form posted
                $response['httpcode'] = Constants::statusEmptyParamsCode;
                $response['message'] = $this->getErrorMessage($response['httpcode'], array(), $this->locale);
            }else{
                //get message catalogue
                $this->catalogue = $this->get('translator')->getCatalogue($this->locale);
                $responseDatasManagement = $this->searchDatas($datas);
                if (is_int($responseDatasManagement)){
                    //return error code
                    $response['httpcode'] = $responseDatasManagement;
                    $response['message'] = $this->getErrorMessage($response['httpcode'], array(), $this->locale);
                }
            }
        }

        if($response['httpcode'] == Constants::statusEverythingIsOkCode){
            $updatedCache = $this->deleteCacheTranslations();
            if(!$updatedCache){
                //ERROR 603 /error deleting the cache
                $response['httpcode'] = Constants::statusErrorDeletingCacheCode;
                $response['message'] = $this->getErrorMessage($response['httpcode'], array(), $this->locale);
            }
        }

        //return response
        return new JsonResponse($response);
    }

    /**
     * @param array $datas
     * @return array
     */
    private function managePostDatas(array $datas){
        $result = [];
        foreach ($datas as $key => $value){
            if ($value != ''){
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * @param array $datas
     * @return bool|int
     */
    private function searchDatas(array $datas){
        $result = true;
        $messages = $this->catalogue->all();
        while ($catalogue = $this->catalogue->getFallbackCatalogue()) {
            $messages = array_replace_recursive($catalogue->all(), $messages);
        }

        //loop messages search in catalogue
        foreach ($messages as $keyMessages => $valueMessages){
            foreach ($datas as $trans => $translation){
                if(key_exists($trans, $valueMessages)){
                    $filename = $keyMessages.'.'.$this->locale.'.xliff';
                    $responseTranslationFileManagement = $this->manageTranslationFile($filename, $trans, $translation);
                    if(is_int($responseTranslationFileManagement)){
                        $result = $responseTranslationFileManagement;
                        return $result;
                    }else{
                        //unset trans
                        unset($datas[$trans]);
                    }
                }
            }
        }

        $count = count($datas);
        //loop missing translations
        if($count != 0){
            foreach ($datas as $trans => $translation){
                $responseTranslationFileManagement = $this->manageTranslationFile($this->filename, $trans, $translation);
                if(is_int($responseTranslationFileManagement)){
                    //ERROR /return error code
                    $result = $responseTranslationFileManagement;
                    return $result;
                }else{
                    //unset trans
                    unset($datas[$trans]);
                }
            }
        }

        $count = count($datas);
        if($count != 0){
            //error 607
            $error_code = 607;
        }

        return $result;
    }

    /**
     * @param $filename
     * @param $trans
     * @param $translation
     * @return bool|int|null
     */
    private function manageTranslationFile($filename, $trans, $translation){
        $result = true;
        //get file with finder
        $document = $this->getXmlFile($this->finder, $this->path, $filename);
        if (!$document) {
            //create the xml file with basic dom based on public/assets/schema.xml
            $document = $this->getXmlFile(
                $this->finder,
                $this->get('kernel')->getRootDir().'/../web/bundles/lusistrans/assets/',
                'schema.xml'
            );
        }
        $responseDomManagement = $this->manageDom($document, $trans, $translation);
        if(is_int($responseDomManagement)){
            //ERROR 602 /error managing the xml dom
            $result = $responseDomManagement;
        }else{
            $responseFileWriting = $this->saveXmlFile($responseDomManagement, $this->path, $filename);
            if(!$responseFileWriting){
                //ERROR 600 /error writing file
                $result = 600;
            }
        }
        return $result;
    }

    /**
     * @param Finder $finder
     * @param $path
     * @param $filename
     * @return bool|string
     */
    private function getXmlFile(Finder $finder, $path, $filename){
        $result = false;
        $finder->files()->in($path);
        foreach ($finder as $file) {
            if ($file->getRelativePathname() == $filename){
                $result = $file->getContents();
            }
        }
        return $result;
    }

    /**
     * @param $document
     * @param $key
     * @param $value
     * @return bool|\DOMDocument
     */
    private function manageDom($document, $key, $value){
        $newDom = new \DOMDocument();
        $newDom->validateOnParse = true;
        $newDom->preserveWhiteSpace = false;
        $newDom->formatOutput = true;
        $newDom->loadXML($document);
        $parent = $newDom->getElementsByTagName('body')->item(0);
        $targetLanguageBlock = $newDom->getElementsByTagName('file')->item(0);
        try{
            $targetLanguageBlock->setAttribute('target-language', $this->locale);
            $node = $this->getElementById($key, $newDom);
            if ($node  != null){
                //update translation form this node
                $newNodeLastChild = $node->getElementsByTagName('target')->item(0);
                $newNodeLastChild->nodeValue = $value;
                $result = $newDom;
            }else{
                //create new node
                $newNode = $newDom->createElement('trans-unit');
                $newNodeFirstChild = $newDom->createElement('source');
                $newNodeFirstChild->nodeValue = $key;
                $newNodeLastChild = $newDom->createElement('target');
                $newNodeLastChild->nodeValue = $value;
                $newNode->setAttribute('id', $key);
                $newNode->appendChild($newNodeFirstChild);
                $newNode->appendChild($newNodeLastChild);
                $parent->appendChild($newNode);
                $result = $newDom;
            }
        }catch(\Exception $e){
            $result = 602;
        }
        return $result;
    }

    /**
     * @param $updatedCrawler
     * @param $path
     * @param $filename
     * @return bool
     */
    private function saveXmlFile($updatedCrawler, $path, $filename){
        try{
            $updatedCrawler->save($path.$filename);
            $result = true;
        }catch(\Exception $e){
            $result = false;
        }
        return $result;
    }

    /**
     * @return bool
     */
    private function deleteCacheTranslations(){
        try{
            $filesystem   = $this->container->get('filesystem');
            $realCacheDir = $this->container->getParameter('kernel.cache_dir').'/translations';
            $this->container->get('cache_clearer')->clear($realCacheDir);
            $filesystem->remove($realCacheDir);
            $result = true;
        }catch(\Exception $e){
            $result = false;
        }
        return $result;
    }

    /**
     * @param $id
     * @param $document
     * @return mixed
     */
    private function getElementById($id, $document){
        $xpath = new \DOMXPath($document);
        return $xpath->query("//*[@id='$id']")->item(0);
    }

    /**
     * @param int $error
     * @param array $params
     * @param $locale
     * @return mixed
     */
    private function getErrorMessage(int $error, Array $params, $locale){
        $message = $this->get('translator')->trans('error.ltb.'.$error, array(), 'errors', $locale);
        return $this->get('translator')->trans($message, $params);
    }
}